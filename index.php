<?
/**
 * Front Controller + Bootstrap
 * Centralizar qualquer requisi��o e direcionar o processamento
 */




/**
 * Enviar o conte�do para o browser compactado com gzip
 */
ob_start('ob_gzhandler');

/**
 * Autoload das classes do sistema
 */
require_once("core/Loader.php");
new \Core\Loader();

/**
 * Atalhos para utiliza��o dos namespaces
 */

use \Core\Config          as Config;
use \Core\Router          as Router;
use \Core\Controller      as Controller;
use \Core\Helper          as Helper;


Router::connect('/Servico/:part/:part:any', '/Servico/$1/Action/$2$3');

/**
 * Handler de erros
 */

ini_set( 'display_errors', 1 );
error_reporting( -1 );
set_error_handler( array( '\Core\Error', 'captureNormal' ) );
set_exception_handler( array( '\Core\Error', 'captureException' ) );
register_shutdown_function( array( '\Core\Error', 'captureShutdown' ) );


/**
 * Defini��o de vari�veis do sistema (core)
 */

preg_match ("!(?P<host>(?:(?P<subdomain>[\w]+)\.)?" . "(?P<domain>\w+)" . "\.(?P<extension>[\w\.]+))!", $_SERVER['HTTP_HOST'], $url );

Config::write('PROTOCOL'      , !isset($_SERVER['HTTPS']) ? 'http://':'https://' );
Config::write('HOST'          , $_SERVER['HTTP_HOST']                            );
Config::write('PHOST'         , Config::read('PROTOCOL').Config::read('HOST')    );
Config::write('DNAME'         , $url['domain']                                   );
Config::write('DOMAIN'        , $url['domain'].".".$url['extension']             );
Config::write('DOCUMENT_ROOT' , $_SERVER['DOCUMENT_ROOT']                        );
Config::write('REQUEST'       , Router::getRoute($_SERVER["REQUEST_URI"])        ); // Roteamento

Router::getRoute($_SERVER["REQUEST_URI"]);
ini_set('session.cookie_domain', ".".Config::read('DOMAIN'));


/**
 * Parser da requisi��o http e montagems dos parametros GET
 */

$path             = explode("/", Config::read('REQUEST'), 4);
$controllerName   = isset($path[1]) ? $path[1]: '';
$actionName       = isset($path[2]) ? $path[2]: '';
$params           = isset($path[3]) ? $path[3]: '';

$params           = explode("/", $params);

$c                = count($params)-1;
$i                = 0;
while($i<$c) {
	$_GET[$params[$i++]] = $params[$i++];
}

$controllerParams = $_GET;



/**
 * Direcionamento da requisi��o ao Controller de destino
 */

if($controllerName == ""){
	$controllerName = "Home";
}

if(!file_exists("app/controllers/".$controllerName.".php")){
	$controllerName = "Error";
	$actionName     = "Index";
}

/**
 * O Controller Index � utilizado como ponto central da aplica��o
 * Nele pode-se definir vari�veis e parametriza��es da aplica��o (app)
 */
if(file_exists("app/controllers/Index.php")){
	Controller::Show('Index/Index', $controllerParams);
}




/**
 * Chamada est�tica do Controller
 */

Controller::Show($controllerName."/".$actionName, $controllerParams);
?>