/**
 * Created by Igor on 26/04/2016.
 */
$(function () {
    "use strict";


    function getFormData(){
        var obj = {};
        $(".form-control").each(function() {
            obj[$(this).attr("id")] = $(this).val()
        });
        return JSON.stringify(obj);
    }






    $('#cnpj').typeahead({
        source: function (query, process) {
            return $.getJSON(
                '/Servico/Cliente/SugestaoCnpj/',
                { cnpj: query },
                function (obj) {
                    return process(obj.data);
                });
        }
    });

    $('#razao_social').typeahead({
        source: function (query, process) {
            return $.getJSON(
                '/Servico/Cliente/SugestaoRazaoSocial/',
                { razao_social: query },
                function (obj) {
                    return process(obj.data);
                });
        }
    });

    $('#conglomerado').typeahead({
        source: function (query, process) {
            return $.getJSON(
                '/Servico/Cliente/SugestaoConglomerado/',
                { nome: query },
                function (obj) {
                    return process(obj.data);
                });
        }
    });

    $('#cdc').typeahead({
        source: function (query, process) {
            return $.getJSON(
                '/Servico/Cliente/SugestaoCentroDecisao/',
                { nome: query },
                function (obj) {
                    return process(obj.data);
                });
        }
    });


    $.get('/Servico/Cliente/Sugestao', function(obj){

        $.each(obj.data.tipo_atendimento, function (i, item) {
            $('#tipo_atendimento').append($('<option>', {
                value: item,
                text : item
            }));
        });

        $.each(obj.data.tipo_classificacao, function (i, item) {
            $('#atendimento').append($('<option>', {
                value: item,
                text : item
            }));
        });

        $(".select2").select2();

    },'json');

    //Initialize Select2 Elements
    $(".select2").select2();

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });


    var dt = '';

    $("[data-widget='eraser']").click(function() {
        $(".form-control").each(function() {
            if($(this).data('select2')){
                $(this).select2("val", "");
            } else {
                $(this).val('');
            }
        });
        if(dt!=''){
            $("#resultado_consulta_cliente").addClass('hidden');
            dt.destroy();
        }
    });


    $('#consultar_cliente').on('click', function (e) {
        waitingDialog.show('Carregando...',{progressType: 'success'});
        var json = getFormData();

        dt = $('#consultar_cliente_resultado').on('xhr.dt', function ( e, settings, json, xhr ) {
            waitingDialog.hide();
            $("#resultado_consulta_cliente").removeClass('hidden');
        }).DataTable({
            "destroy": true,
            "scrollX": true,
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "autoWidth": true,
            deferRender:    true,
            "paging": true,
            "pageLength": 10,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "dom": 'Bfrtip',
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }],
            buttons: [
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-download"></i> Exportar',
                    titleAttr: 'Excel',
                    title    : 'consultar_cliente_resultado'
                },
            ],


            "ajax":{
                "url": "/Servico/Cliente/Consultar",
                "type": "POST",
                "data" : {json: json}
            },
            "columns": [
                { "data": "CNPJ" },
                { "data": "RazaoSocial" },
                { "data": "CentroDecisao" },
                { "data": "Conglomerado" },
                { "data": "TipoAtendimento" },
                { "data": "TipoClassificacao" },
                { "data": "DiretorVendas" },
                { "data": "GerenteVendas" },
                { "data": "Regional" },
                { "data": "ExecutivoNegocios" },
                { "data": "Especialista1" },
                { "data": "Especialista2" },
                { "data": "PreVendas" },
                { "data": "PosVendas" }
            ],
            "language" : {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });




    });
});