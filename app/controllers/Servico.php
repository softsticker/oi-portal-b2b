<?
namespace Controllers;

class Servico extends \Core\Controller {

	public function __call($obj, $args)
	{
		if($obj != '') {
			$obj     = "\\Models\\BO\\".$obj;

			$Action  = $this->getParam('Action');

			if(method_exists($obj,$Action)){

				if(isset($_POST['json'])){
					$json = json_decode($_POST['json']);
					foreach($json as $json_id => $json_value){
						$_POST[$json_id] = $json_value;
						$this->Params[$json_id] = $json_value;
					}
					unset($_POST['json']);
				}

				$Servico = new $obj;

				foreach($this->Params as $Param=>$Value){
					if($Param != 'Action'){
						$Servico->{$Param} = $Value;
					}
				}

				$result  = call_user_func(array($Servico, $Action));

				header('Content-Type: application/json');
				if(count($result) == 0){
					$result = false;
				}
				$result = array_merge(array('success' => true), array('data' => $result));
				echo $this->json_encode($result);
			} else{
				$result = array('success' => false, 'error' => 'Serviço não disponível');
				echo $this->json_encode($result);
			}
		} else {
			// TODO: retornar erro
		}
	}
}
?>