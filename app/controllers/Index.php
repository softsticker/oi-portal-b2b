<?

namespace Controllers;
use Core\Config as Config;


/**
*  Class Controller Index
*
*  É a classe responsável por centralizar as parametrizações da aplicação
*/

class Index extends \Core\Controller {

	
	/**
	* Método chamado automaticamente pelo framework
	*/
	public function Index() {
		self::Ambiente();
		self::DataBase();
	}
	
	
	
	/**
	* Parametrizações de ambiente
	*/
	public static function Ambiente() {
		if($_SERVER['SERVER_ADDR'] == '10.0.2.15'){
			Config::write('ENV' , 'DEV');
		} else {
			Config::write('ENV' , 'PRD');
		}
	}
	
	
	/**
	* Parametrizações de acesso ao Banco de dados
	*/
	public static function DataBase() {

		$Env = Config::read('ENV');

		if($Env == 'DEV'){
			$Db_Master = (object) array (
				'HOST' => '127.0.0.1',
				'PORT' => '3306',
				'USER' => 'root',
				'PASS' => '163384se',
				'BASE' => 'PortalB2B'
			);

			$Db_Slave = (object) array (
				'HOST' => '127.0.0.1',
				'PORT' => '3306',
				'USER' => 'root',
				'PASS' => '163384se',
				'BASE' => 'PortalB2B'
			);

		} else if ($Env == 'PRD'){
			$Db_Master = (object) array (
				'HOST' => '127.0.0.1',
				'PORT' => '3306',
				'USER' => 'u208088104_oib2b',
				'PASS' => '163384se',
				'BASE' => 'u208088104_oib2b'
			);

			$Db_Slave = (object) array (
				'HOST' => '127.0.0.1',
				'PORT' => '3306',
				'USER' => 'u208088104_oib2b',
				'PASS' => '163384se',
				'BASE' => 'u208088104_oib2b'
			);
		}

		Config::write('DB_MASTER' , $Db_Master);
		Config::write('DB_SLAVE'  , $Db_Slave );
		
	}

	
	public function Error() {}
}

?>