<?
namespace Models\BO;

use Models\DAO\Conglomerado;

class Cliente extends \Core\Object {

    public function __construct() {
    }

    public function SugestaoCnpj(){
        $Cliente       = new \Models\DAO\Cliente();
        $Cliente->CNPJ = $this->cnpj;
        $DadosCliente  = $Cliente->getCliente(10);
        return $DadosCliente;
    }

    public function SugestaoRazaoSocial(){
        $Cliente              = new \Models\DAO\Cliente();
        $Cliente->RazaoSocial = $this->razao_social;
        $DadosCliente         = $Cliente->getCliente(10);
        return $DadosCliente;
    }

    public function SugestaoConglomerado(){
        $Conglomerado       = new \Models\DAO\Conglomerado();
        $Conglomerado->Nome = $this->nome;
        $DadosConglomerado  = $Conglomerado->getConglomerado(10);
        return $DadosConglomerado;
    }

    public function SugestaoCentroDecisao(){
        $CentroDecisao       = new \Models\DAO\CentroDecisao();
        $CentroDecisao->Nome = $this->nome;
        $DadosCentroDecisao  = $CentroDecisao->getCentroDecisao(10);
        return $DadosCentroDecisao;
    }

    public function Sugestao(){

        $TiposAtendimento   = '';
        $TiposClassificacao = '';

        $TipoAtendimento      = new \Models\DAO\TipoAtendimento();
        $DadosTipoAtendimento = $TipoAtendimento->getTipoAtendimento();
        foreach($DadosTipoAtendimento as $TA){
            $TiposAtendimento[] = $TA->Nome;
        }

        $TipoClassificacao      = new \Models\DAO\TipoClassificacao();
        $DadosTipoClassificacao = $TipoClassificacao->getTipoClassificacao();
        foreach($DadosTipoClassificacao as $TC){
            $TiposClassificacao[] = $TC->Nome;
        }

        $obj                      = new \stdClass();
        $obj->tipo_atendimento    = $TiposAtendimento;
        $obj->tipo_classificacao  = $TiposClassificacao;

        return $obj;
    }

    public function Consultar(){

        $Cliente                    = new \Models\DAO\Cliente();
        $Cliente->RazaoSocial       = $this->razao_social;
        $Cliente->CentroDecisao     = $this->cdc;
        $Cliente->Conglomerado      = $this->conglomerado;
        $Cliente->TipoAtendimento   = $this->tipo_atendimento;
        $Cliente->TipoClassificacao = $this->atendimento;

        /* TO DO: Montar string para consultar usuarios */
        $Cliente->TimeContas = $this->executivo_negocios;
        $Cliente->TimeContas = $this->executivo_especialista_1;
        $Cliente->TimeContas = $this->executivo_especialista_2;
        $Cliente->TimeContas = $this->gerente_vendas;
        $Cliente->TimeContas = $this->diretor_vendas;
        $Cliente->TimeContas = $this->regional;
        $Cliente->TimeContas = $this->pos_vendas;
        $Cliente->TimeContas = $this->pre_vendas;
        $Cliente->TimeContas = $this->outros;

        $Clientes             = $Cliente->getClienteTimeConta();

        return $Clientes;
    }

}
?>