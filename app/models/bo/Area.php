<?
namespace Models\BO;

class Area extends \Core\Object {

    public function __construct()
    {
    }

    public function getMenu(){
        $Area = new \Models\DAO\Area();
        $Menu = $Area->getMenu();


        $MenuArray = null;

        if(count($Menu) > 0){
            foreach($Menu as $M){
                if($M->AreaAsc == null){
                    $MenuArray[$M->AreaId] = $M;
                } else {
                    $MenuArray[$M->AreaAsc]->Area[$M->AreaId] = $M;
                }

            }
        }

        return $MenuArray;
    }
}
?>