<?
namespace Models\DAO;

class Cliente extends \Core\Model {

    public function __construct() {	}

    public function getCliente($Limit='') {


        $sql_limit = '';
        if($Limit != ''){
            $sql_limit = " LIMIT $Limit ";
        }

        $sql_comp  = '';
        $sql_field = '';
        if(isset($this->CNPJ) && $this->CNPJ != ''){
            $sql_comp  .= ' AND CNPJ LIKE "%'.$this->CNPJ.'%" ';
            $sql_field .= ', CNPJ ';
        }
        if(isset($this->RazaoSocial) && $this->RazaoSocial != ''){
            $sql_comp  .= ' AND RazaoSocial LIKE "%'.$this->RazaoSocial.'%" ';
            $sql_field .= ', RazaoSocial ';
        }

        $sql_field = ltrim($sql_field, ',');

        if($sql_comp != ''){
            $sql_comp = ' WHERE 1=1 '.$sql_comp;
        }

        if($sql_comp != '' && $sql_field != ''){
            $sql = "SELECT DISTINCT $sql_field
                FROM Cliente
                $sql_comp
                ORDER BY $sql_field
                $sql_limit
                ";
            return $this->getResult($sql, 'value');
        } else {
            return null;
        }

        // TODO: CACHE como parametro
    }


    public function getClienteTimeConta(){

        $sql_comp = '';

        if($this->RazaoSocial != ''){
            $sql_comp .= " AND C.RazaoSocial LIKE '$this->RazaoSocial%' ";
        }

        if($this->CentroDecisao != ''){
            $sql_comp .= " AND CD.Nome LIKE '$this->CentroDecisao%' ";
        }

        if($this->Conglomerado != ''){
            $sql_comp .= " AND CM.Nome LIKE '$this->Conglomerado%' ";
        }

        if($this->TipoAtendimento != null && count($this->TipoAtendimento) > 0){
            $in = '';
            foreach($this->TipoAtendimento as $TipoAtendimento){
                $in .= "'".$TipoAtendimento."',";
            }
            $in = rtrim($in, ',');
            $sql_comp .= " AND TA.Nome IN ($in) ";
        }

        if($this->TipoClassificacao != null && count($this->TipoClassificacao) > 0){
            $in = '';
            foreach($this->TipoClassificacao as $TipoClassificacao){
                $in .= "'".$TipoClassificacao."',";
            }
            $in = rtrim($in, ',');
            $sql_comp .= " AND TC.Nome IN ($in) ";
        }


        if($sql_comp != ''){

            $sql_comp = substr($sql_comp, 4);

            $sql = "SELECT  C.ClienteId,
                            C.RazaoSocial,
                            C.CNPJ,
                            CD.Nome as CentroDecisao,
                            CM.Nome as Conglomerado,
                            TA.Nome as TipoAtendimento,
                            TC.Nome as TipoClassificacao,
                            '' as DiretorVendas,
                            '' as GerenteVendas,
                            '' as Regional,
                            '' as ExecutivoNegocios,
                            '' as Especialista1,
                            '' as Especialista2,
                            '' as PreVendas,
                            '' as PosVendas
                    FROM Cliente C INNER JOIN CentroDecisao CD ON C.CentroDecisaoId = CD.CentroDecisaoId
                    INNER JOIN Conglomerado CM ON C.ConglomeradoId = CM.ConglomeradoId
                    INNER JOIN TipoAtendimento TA ON C.TipoAtendimentoId = TA.TipoAtendimentoId
                    INNER JOIN TipoClassificacao TC ON C.TipoClassificacaoId = TC.TipoClassificacaoId
                    WHERE $sql_comp";
            return $this->getResult($sql);

        }
        return null;
    }


}

?>