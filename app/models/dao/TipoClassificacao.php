<?
namespace Models\DAO;

class TipoClassificacao extends \Core\Model {

    public function __construct() {	}

    public function getTipoClassificacao() {
        $sql = "SELECT TipoClassificacaoId, Nome
                FROM TipoClassificacao
                ORDER BY TipoClassificacaoId";
        return $this->getResult($sql);
    }
}

?>