<?
namespace Models\DAO;

class Area extends \Core\Model {

    public function __construct() {	}

    public function getMenu() {
       $sql = "SELECT *
               FROM
                Area
               WHERE
                Status = 1
               ORDER BY
                AreaAsc, AreaId
                ";
       return $this->getResult($sql);
    }
}

?>