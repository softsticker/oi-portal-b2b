<?
namespace Models\DAO;

class CentroDecisao extends \Core\Model {

    public function __construct() {	}

    public function getCentroDecisao($Limit='') {

        $sql_limit = '';
        if($Limit != ''){
            $sql_limit = " LIMIT $Limit ";
        }

        $sql_comp  = '';
        $sql_field = '';
        if(isset($this->Nome) && $this->Nome != ''){
            $sql_comp  .= ' AND Nome LIKE "%'.$this->Nome.'%" ';
            $sql_field .= ', Nome ';
        }

        $sql_field = ltrim($sql_field, ',');

        if($sql_comp != ''){
            $sql_comp = ' WHERE 1=1 '.$sql_comp;
        }

        if($sql_comp != '' && $sql_field != '') {

            $sql = "SELECT $sql_field
                    FROM CentroDecisao
                    $sql_comp
                    ORDER BY $sql_field
                    $sql_limit";
            return $this->getResult($sql, 'value');
        } else {
            return null;
        }
    }
}

?>