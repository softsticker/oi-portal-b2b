<?
namespace Models\DAO;

class TipoAtendimento extends \Core\Model {

    public function __construct() {	}

    public function getTipoAtendimento() {
        $sql = "SELECT TipoAtendimentoId, Nome
                FROM TipoAtendimento
                ORDER BY TipoAtendimentoId";
        return $this->getResult($sql);
    }
}

?>