<?
namespace Core;

/**
*  Class Router
*
*  � a classe respons�vel pela defini��o das rotas do framework
*/
class Router extends Object {

	private $routes;
	private static $instance;
	
	/**
	*  Retorna uma �nica inst�ncia (Singleton) da classe solicitada.
	*
	*  @return Router Objeto da classe utilizada
	*/
    public static function getInstance() {
       if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
    
    
    /**
    *  Criar uma rota para uma determinada url
    *
    *  @param string $url   url requisitada
    *  @param string $route rota a ser seguida
    *  @return string 
    */
    public static function connect($url = "", $route = "") {
        if(is_array($url)):
            foreach($url as $key => $value):
                Router::connect($key, $value);
            endforeach;
            return true;
        elseif($url !== null):
            $self = Router::getInstance();
            $url = rtrim($url, "/");
            return $self->routes[$url] = rtrim($route, "/");
        endif;
        return false;
    }

    /**
    *  Realizar o parser da requisi��o e retornar a rota correspondente
    *  
    *  @param string $url url requisitada
    *  @return string 
    */
    public static function getRoute($url) {
        $self = Router::getInstance();
  		if(count($self->routes) > 0):
	        foreach($self->routes as $map => $route):
	            $map = "/^" . str_replace(array("/", ":any", ":part", ":num"), array("\/", "(.*)", "([^\/]*)", "([0-9]+)"), $map) . "\/?$/";
	            $url = preg_replace($map, $route, $url);
	        endforeach;
  		endif;
        return rtrim($url, "/");
    }
}

?>