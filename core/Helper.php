<?php
namespace Core;

/**
 *  Class Helper
 *
 *  � a classe respons�vel pela cria��o dos objetos singleton Helpers
 */
class Helper extends Object {
	private static $instance;

	/**
	 * Construtor padr�o privado
	 */
	private function __construct() {
	}

	/**
	 * M�todo singleton para retornar uma �nica inst�ncia do helper
	 * @param string $Helper nome do helper
	 * @return Helper
	 */
	public static function getInstance($Helper) {
		if (!isset(self::$instance[$Helper])) {
			$Obj = "Core\\Helpers\\".$Helper;
			self::$instance[$Helper] = new $Obj();
		}
		return self::$instance[$Helper];
	}

	/**
	 * M�todo est�tico para acesso r�pido � uma classe helper (singleton)
	 * @param string $Helper nome do helper
	 * @return Helper
	 */
	public static function getHelper($Helper) {
		return self::getInstance($Helper);
	}


}

?>