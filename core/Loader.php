<?
namespace Core;

/**
 *  Class Loader
 *
 *  � a classe respons�vel pelo autoload das classes do sistema / aplica��o
 *  atrav�s do uso de namespaces
 */
class Loader {

	/**
	 * Construtor padr�o
	 */
	public function __construct() {
		spl_autoload_extensions(".php");
		spl_autoload_register(function ($class) {

			$class = str_ireplace('Core\\'        , 'core\\'            , $class);
			$class = str_ireplace('Controllers\\' , 'app\\controllers\\', $class);
			$class = str_ireplace('Models\\BO\\'  , 'app\\models\\bo\\' , $class);
			$class = str_ireplace('Models\\DAO\\' , 'app\\models\\dao\\', $class);

			require_once(str_replace('\\', '/', $class . '.php'));
		});
	}

}
?>