<?
namespace Helpers;

/**
 *  Class Date
 *
 *  Helper para auxiliar na manipula��o de datas
 */
class Date extends \DateTime {

	/**
	 *  Imprimir o objeto no formato padr�o
	 *
	 *  @return string       data formatada em 'Y-m-d H:i:s'
	 */
	function __toString() {
		return $this->format('Y-m-d H:i:s');
	}

	/**
	 *  Calcular a diferen�a entre duas datas
	 *
	 *  @param string $firstDate  Data inicial
	 *  @param string $secondDate Data final
	 *  @return DateInterval      Objeto DateInterval com o resultado
	 */
	static function difference($firstDate, $secondDate) {
		$firstDateTimeObj  = new self($firstDate);
		$secondDateTimeObj = new self($secondDate);
		return $firstDateTimeObj->diff($secondDateTimeObj);
	}

}



?>