<?
namespace Helpers;

/**
 *  Class Session
 *
 *  Helper para auxiliar na manipula��o de sess�es, usando o memcache
 */
class Session extends \Core\Helper
{
	/**
	 * Tempo de expira��o da sess�o
	 */
	private static $lifetime = 0;

	/**
	 * Inst�ncia do memcache
	 */
	private $mc;


	/**
	 * Construtor padr�o
	 * Define o handler da session e persistencia do ID da session em cookie
	 */
	public function __construct()	{

		/*$this->mc = \Core\MC::getInstance('M4');

		session_set_save_handler(
		array($this, "open"),
		array($this, "close"),
		array($this, "read"),
		array($this, "write"),
		array($this, "destroy"),
		array($this, "gc")
		);


		if(isset($_COOKIE['Session'])) {
			session_id($_COOKIE['Session']);
			session_start();
		}
		else {
			session_start();
			setcookie("Session", session_id(), time()+(12 * 720 * 3600), '/'); // 1 ano de cookie
		}*/
	}

	/**
	 *  Quando a sess�o for iniciada
	 *
	 *  @return boolean
	 */
	public static function open()	{
		self::$lifetime = ini_get('session.gc_maxlifetime');
		return true;
	}

	/**
	 *  Ler valor da sess�o no memcache
	 *
	 *  @param string $id  id da sess�o
	 *  @return string
	 */
	public function read($id)	{
		return $this->mc->get("sessions/{$id}");
	}

	/**
	 *  Gravar valor da sess�o no memcache
	 *
	 *  @param string $id    id da sess�o
	 *  @param string $data  valor para gravar
	 *  @return boolean
	 */
	public function write($id, $data)	{
		return $this->mc->set("sessions/{$id}", $data, self::$lifetime);
	}

	/**
	 *  Destruir a sess�o
	 *
	 *  @param string $id    id da sess�o
	 *  @return boolean
	 */
	public function destroy($id)	{
		return $this->mc->delete("sessions/{$id}");
	}


	/**
	 *  Gravar valor na sess�o
	 *
	 *  @param string $index  �ndice da sess�o
	 *  @param string $value  valor para gravar na sess�o
	 */
	public static function set($index, $value) {
		$_SESSION[$index] = $value;
	}

	/**
	 *  Ler valor da sess�o
	 *
	 *  @param string $index �ndice da sess�o
	 *  @return string
	 */
	public static function get($index) {
		return isset($_SESSION[$index]) ? $_SESSION[$index] : null;
	}

	/**
	 *  Garbage Collector
	 *
	 *  @return boolean
	 */
	public static function gc(){
		return true;
	}

	/**
	 *  Fechar sess�o
	 *
	 *  @return boolean
	 */
	public static function close(){
		return true;
	}

	/**
	 *  Destrutor
	 *  Quando o objeto de sess�o for destru�do, finalizar a sess�o
	 */
	public function __destruct() {
		session_write_close();
	}
}

?>