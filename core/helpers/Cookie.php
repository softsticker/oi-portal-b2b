<?
namespace Helpers;

/**
 *  Class Cookie
 *
 *  Helper para auxiliar na manipula��o de cookies
 */
class Cookie extends \Core\Object {
	/**
	 * Tempo de expira��o da sess�o
	 */
	private static $lifetime = 31104000;

	/**
	 * Inst�ncia
	 */
	private static $instance;

	/**
	 * Nome do cookie
	 */
	private $name;

	/**
	 * Valor do cookie
	 */
	private $value;


	/**
	 * Construtor padr�o
	 * Define o cookie que ser� manipulado (atrav�s do nome)
	 */
	public function __construct($name)	{
		$this->name = $name;
	}

	/**
	 * M�todo singleton para retornar uma �nica inst�ncia do helper
	 * @param string $Cookie nome do cookie
	 * @return Cookie
	 */
	private static function getInstance($Cookie) {
		if (!isset(self::$instance[$Cookie])) {
			self::$instance[$Cookie] = new self($Cookie);
		}
		return self::$instance[$Cookie];
	}

	/**
	 * M�todo est�tico para acesso r�pido � um cookie
	 * @param string $Cookie nome do cookie
	 * @return Cookie
	 */
	public static function getCookie($Cookie) {
		return self::getInstance($Cookie);
	}

	/**
	 * Ler um valor do cookie
	 * @param string $index �ndice do cookie
	 * @return string
	 */
	public function read($index)	{

		if (isset($this->value[$index])) {
			return $this->value[$index];
		}
		else if(isset($_COOKIE[$this->name])) {
			$cookie              = $this->decode($_COOKIE[$this->name]);
			$this->value[$index] = isset($cookie[$index]) ? $cookie[$index] : null;
			return $this->value[$index];
		}
		return false;
	}

	/**
	 * Gravar um valor no cookie
	 * @param string $index �ndice do cookie
	 * @param string $value valor para gravar no cookie
	 * @return string
	 */
	public function write($index, $value)	{

		if(isset($_COOKIE[$this->name])) {
			$cookie = $this->decode($_COOKIE[$this->name]);
		}
		else
			$cookie = array($index=>$value);

		$cookie[$index]      = $value;
		$this->value[$index] = $value;

		$cookie = $this->encode($cookie);
		setcookie($this->name, $cookie, time()+(self::$lifetime), '/'); // 1 ano de cookie

	}

	/**
	 * Codificar o cookie para grava��o
	 * @param array $array array do cookie
	 * @return string      string serializada e codificada com base64
	 */
	public function encode($array) {
		return base64_encode(serialize($array));
	}

	/**
	 * Decodificar o cookie para leitura
	 * @param string $string string serializada e codificada com base64
	 * @return array         array do cookie
	 */
	public function decode($string) {
		return unserialize(base64_decode($string));
	}

}

?>