<?
namespace Helpers;

/**
 *  Class Number
 *
 *  Helper para auxiliar na manipula��o de n�meros
 */
class Number extends \Core\Helper {

	/**
	 *  Formatar pre�o
	 *
	 *  @param float $Preco  pre�o sem formata��o
	 *  @return string       pre�o formatado com R$
	 */
	public function Preco($Preco) {
		$Preco = number_format($Preco, 2, ",", ".");
		return trim("R$ " .$Preco);
	}
}

?>