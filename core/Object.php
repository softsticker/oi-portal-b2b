<?
namespace Core;

/**
 *  Class Object
 *
 *  � a classe respons�vel pela defini��o padr�o de um objeto
 *  Define os m�todos m�gicos do php
 */
abstract class Object {

	/**
	 * M�todo m�gico para auxiliar no encapsulamento dos atributos
	 * Quando um atributo de objeto for acessado diretamente, ser� chamado
	 * automaticamente um metodo set{$Atributo} da classe ($this)
	 *
	 * @param string $property atributo que ser� setado
	 * @param string $value    valor do atributo que ser� setado
	 * @return string
	 */
	public function __set($property, $value) {
		$method = "set{$property}";
		if(method_exists($this, $method)) {
			return $this->$method($value);
		} else {
			return $this->{$property} = $value;
		}

	}

	/**
	 * M�todo m�gico para auxiliar no encapsulamento dos atributos
	 *
	 * @param string $property atributo que ser� consultado
	 * @return string
	 */
	public function __get($property) {
		$property = "{$property}";
		return $this->$property;
	}

	/**
	 * M�todo m�gico para imprimir um objeto
	 *
	 * @return string
	 */
	public function __toString() {
		$x = "<pre>";
		$x .= print_r($this, true);
		$x .= "</pre>";
		return $x;
	}

	/**
	 * M�todo m�gico executado quando um m�todo � chamado
	 *
	 * @param string $name      m�todo que ser� chamado
	 * @param string $arguments argumentos do m�todo
	 *
	 * @return string
	 */
	public function __call($name, $arguments) {
	}


	/**
	 * M�todo m�gico executado quando um m�todo est�tico � chamado
	 *
	 * @param string $name      m�todo que ser� chamado
	 * @param string $arguments argumentos do m�todo
	 *
	 * @return string
	 */
	public static function __callStatic($name, $arguments) {
	}


	/**
	 * Converte um objeto em um array associativo
	 *
	 * @return array
	 */
	public function object_to_array($var) {
		$result = array();
		$references = array();
		$data = null;
		foreach ($var as $key => $value) {
			if (is_object($value) || is_array($value)) {
				if (!in_array($value, $references)) {
					$data -> {utf8_encode($key)} = $this->object_to_array($value);
					$references[] = $value;
				}
			} else {
				$data -> {utf8_encode($key)} = utf8_encode($value);
			}
		}
		return $data;
	}


	/**
	 * Converte um valor para JSON
	 *
	 * @return string
	 */
	public function json_encode($param) {
		$retorno = null;
		if (is_object($param) || is_array($param)) {
			$param = $this->object_to_array($param);
			if (count($param)>0) {
				foreach($param as $data) {
					$retorno[] = $data;
				}
			}
		}
		if ($retorno != null)
			return json_encode($retorno);
		else
			return 'false';
	}

}

?>