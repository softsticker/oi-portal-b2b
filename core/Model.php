<?
namespace Core;

/**
 *  Class Model
 *
 *  � a classe respons�vel pela comunica��o com o banco de dados
 */
class Model extends \Mysqli {

	private static $instance;

	/**
	 * Construtor padr�o
	 * Realiza a conex�o com o banco de dados
	 *
	 * @param string $type indica qual a inst�ncia do banco ser� instanciada (master ou slave)
	 */
	public function __construct($type) {
		$ConfigDB = strtoupper("DB_".$type);
		$Database = Config::read($ConfigDB);

		@parent::__construct($Database->HOST,
		$Database->USER,
		$Database->PASS,
		$Database->BASE,
		$Database->PORT );

		if( $this->connect_errno ) {
			throw new \Exception($this->connect_error, $this->connect_errno);
		}
	}

	/**
	 * Criar / retornar inst�ncia do banco (singleton)
	 *
	 * @param string $type indica qual a inst�ncia do banco deve ser instanciada
	 * @return Model
	 */
	private static function getInstance($type) {
		if( !isset(self::$instance[$type]) ) {
			self::$instance[$type] = new self($type);
		}
		return self::$instance[$type];
	}

	/**
	 * Executar um comando sql no banco
	 *
	 * @param string $sql  comando sql
	 * @param string $erro se ocorrer algum erro ele ser� retornado por refer�ncia
	 * @return Model
	 */
	public function execute($sql, &$erro=null) {


		# se for select use o banco slave (leitura)
		if(preg_match("#(select)#is", $sql)) {
			$inst  = self::getInstance('Slave');
		}
		# se n�o for select use o banco master (escrita)
		else {
			$inst  = self::getInstance('Master');
		}
	
		$query = $inst->query($sql);
	
		if(!$query) {
			$erro = $inst->error;
		}
		return $query;
	}

	/**
	 * Retornar linhas do banco de dados
	 *
	 * @param string $sql  comando sql
	 * @param string $return_type retornar object, array, etc
	 * @param string $erro se ocorrer algum erro ele ser� retornado por refer�ncia
	 * @return Model
	 */
	public function getResult($sql, $return_type='object', &$erro=null) {

		$result = null;
		if($query  = $this->execute($sql, $erro)) {

			if($return_type == 'object') {
				while($r = $query->fetch_object())
				{
					$result[] = $r;
				}
			} else if ($return_type == 'value'){
				while($r = $query->fetch_array(MYSQLI_NUM))
				{
					$result[] = $r[0];
				}
			}


			return $result;
		} else {
			echo $erro;
			return null;
		}

	}

	/**
	 * Retornar quantidade de linhas de uma consulta
	 *
	 * @param string $sql  comando sql
	 * @return int
	 */
	public function getNumRows($sql) {
		return $this->execute($sql)->num_rows;
	}

	/**
	 * Iniciar uma transa��o no banco de dados
	 */
	public function startTransaction() {
		$this->autocommit(false);
	}



}
?>