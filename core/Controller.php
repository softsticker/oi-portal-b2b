<?
namespace Core;

/**
 * Class Controller
 *
 * Essa classe implementa a camada controller do sistema
 * respons�vel pelo tratamento das requisi��es.
 *
 */
class Controller extends Object {

	public $Name;
	public $Action;
	public $Params;
	public static $View;

	/**
	 * Construtor padr�o
	 */
	public function __construct(){
		$this->View               = View::getInstance();
		//$this->View->Controller   = $this;
	}

	/**
	 * Executa a action chamada pelo controller
	 * @param boolean $Return se estiver setada significa que espera-se um retorno dessa chamada
	 */
	private function Run($Return = false){
			
		$actionToCall = $this->Action;
		if($this->Action == ''){
			$actionToCall = 'Index';
		}
		if(!is_callable(array(&$this, $actionToCall))){
			$actionToCall = 'Error';
		}
		if($Return){
			return call_user_func(array(&$this, $actionToCall));
		}
		else{
			call_user_func(array(&$this, $actionToCall));
		}
	}

	/**
	 * Retorna o valor de uma vari�vel passa na requisi��o HTTP/GET
	 * @param int $index �ndice/nome da vari�vel
	 * @return string
	 */
	public function getParam($index){
		$retorno = isset($this->Params[$index]) ? trim($this->Params[$index]) : null;
		return $retorno;
	}

	/**
	 * Interface de acesso r�pido � uma classe helper (singleton) via controller
	 * @param string $Helper nome do helper
	 * @return Helper
	 */
	public function Helper($Helper){
		return Helper::getHelper($Helper);
	}

	/**
	 * M�todo para iniciar a execu��o de controller
	 * @param string   $Path caminho do controller
	 * @param string[] $Param array de parametros para o controller
	 * @param boolean  $Return flag indicando se espera ou n�o um retorno dessa execu��o
	 * @return Helper
	 */
	public static function Show($Path, $Param=null, $Return = false){

		$Path               = explode("/", $Path);
		$Obj                = "Controllers\\".$Path[0];

		$Controller 		= new $Obj();
		$Controller->Name   = $Path[0];
		$Controller->Action = $Path[1];
		$Controller->Params = $Param;

		if($Return){
			return $Controller->Run(true);
		}else{
			$Controller->Run();
		}
	}

	/**
	 * Interface de acesso r�pido � uma controller que deve retornar
	 * @param string   $Path caminho do controller
	 * @param string[] $Param array de parametros para o controller
	 * @return Controller
	 */
	public static function Call($Path, $Param=null){
		return self::Show($Path, $Param, true);
	}

	function json_encode($arr) {
		\array_walk_recursive($arr, function (&$item, $key) { if (is_string($item)) $item = \mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8'); });
		return \mb_decode_numericentity(json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');
	}
	
}
?>