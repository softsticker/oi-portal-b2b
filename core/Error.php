<?
namespace Core;

/**
 *  Class Error
 *
 *  � a classe respons�vel pela captura dos erros de execu��o do sistema
 */
class Error extends Object {

	/**
	 * Erros que podem ser capturados (catch)
	 *
	 * @param int    $number c�digo do erro
	 * @param string $message mensagem de erro
	 * @param string $file arquivo que apresentou o erro
	 * @param int    $line linha onde ocorreu o erro
	 */
	public static function captureNormal( $number, $message, $file, $line )	{

		/**
		 * Montar array do erro
		 */
		$error = array( 'type' => $number, 'message' => $message, 'file' => $file, 'line' => $line );

		self::printError($error);
	}

	/**
	 * Exce��es
	 *
	 * @param string $exception exce��o capturada
	 */
	public static function captureException( $exception ) {

		self::printError($exception);
	}

	/**
	 * Erros n�o captur�veis
	 */
	public static function captureShutdown( ) {

		$error = error_get_last( );
		if( $error ) {
			self::printError($error);
		}
		else { return true;
		}
	}


	/**
	 * Imprimir o erro
	 * @param array $info array contendo informa��es do erro
	 */
	public static function printError($info) {

		/**
		 * Exibir o erro
		 */
		echo '<pre>';
		print_r( $info );
		echo '<hr>';
		debug_print_backtrace();
		echo '</pre>';
		
		

	}
}





?>