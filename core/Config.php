<?
namespace Core;

/**
 *  Class Config
 *
 *  É a classe que toma conta de todas as configurações necessárias para
 *  o sistema / aplicação
 */
class Config extends Object {

	private static $instance;
	
	/**
	 *  Definições de configurações.
	 *
	 *  @var array
	 */
	private $config = array();

	/**
	 *  Retorna uma única instância (Singleton) da classe solicitada.
	 *
	 *  @staticvar object $instance Objeto a ser verificado
	 *  @return object Objeto da classe utilizada
	 */
    public static function getInstance() {
       if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }

	/**
	 *  Retorna o valor de uma determinada chave de configuração.
	 *
	 *  @param string $key Nome da chave da configuração
	 *  @return mixed Valor de configuração da respectiva chave
	 */
	public static function read($key = "") {
		$self = self::getInstance();
		return isset($self->config[$key]) ? $self->config[$key] : null;
	}

	/**
	 *  Grava o valor de uma configuração da aplicação para determinada chave.
	 *
	 *  @param string $key Nome da chave da configuração
	 *  @param string $value Valor da chave da configuração
	 *  @return boolean true
	 */
	public static function write($key = "", $value = "") {
		$self = self::getInstance();
		$self->config[$key] = $value;
		return true;
	}
}

?>