<?
namespace Core;

/**
 *  Class View
 *
 *  � a classe respons�vel pela exibi��o das informa��es atrav�s de templates
 */
class View extends Object {

	private static $instance;

	private static $htmlCode;

	private static $vars;

	/**
	 * Construtor privado (singleton)
	 */
	private function __construct(){
		ob_start();
	}


	/**
	* Encapsulamento dos atributos
	* Quando um atributo de objeto for acessado diretamente, ser� chamado
	* automaticamente um metodo set{$Atributo} da classe ($this)
	*
	* @param string $property atributo que ser� setado
	* @param string $value    valor do atributo que ser� setado
	* @return string
	*/
	public function setVar($property, $value) {
		self::$vars[$property] = $value;
	}
	
	/**
	 * M�todo m�gico para auxiliar no encapsulamento dos atributos
	 *
	 * @param string $property atributo que ser� consultado
	 * @return string
	 */
	public function getVar($property=null) {
		if ($property == null) return self::$vars;
		else return isset(self::$vars[$property]) ? self::$vars[$property] : null;
	}
	
	/**
	 * M�todo singleton para retornar uma �nica inst�ncia da view
	 *
	 * @return View
	 */
	public static function getInstance() {

		if (!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	
	/**
	* M�todo para realizar o processamento do template
	* @param string  $template path da view
	*/
	public function Show($template, $full=false) {

		$phtml        = 'app/views/'.$template.'.phtml';

		if (!file_exists($phtml)) throw new \InvalidArgumentException("arquivo $phtml n�o existe");

		if($full){
			$this->setVar('Area', $template);
			$this->Show('Page/Index');
		} else {
			include($phtml);
		}


	}
	
	
	/**
	* M�todo para comprimir a saida html
	* @param string  $code c�digo fonte original
	* @return string       c�digo fonte comprimido
	*/
	private function compress($code)
	{
		return preg_replace(
		    array(
		        '/ {2,}/',
		        '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'
		    ),
		    array(
		        ' ',
		        ''
		    ),
		    $code
		);
	}
		

	/**
	* M�todo para realizar a exibi��o do template, utilizado no destrutor do objeto singleton
	* momento em que todas as views foram processadas e consolidadas no buffer de sa�da
	*/
	public function __destruct() {

		self::$htmlCode = ob_get_contents();
		ob_end_clean();
		echo self::$htmlCode;
		//echo self::compress(self::$htmlCode);


	}

}
?>